# haXlab Toolkit - GE Z-Wave Smart Plug
Please visit https://haxlab.atlassian.net/wiki/spaces/GZSP/overview for the most up to date documentation.

The docs folder contains all the documents for this fixture

The scripts folder contains the scripts found in ~/GeZwavePlug
